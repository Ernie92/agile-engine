import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import App from './src/app';
import { name as appName } from './app.json';
import { store } from './src/store';

const AgileEngine = () => (
	<Provider store={store}>
		<App />
	</Provider>
);

AppRegistry.registerComponent(appName, () => AgileEngine);
