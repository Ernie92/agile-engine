#AgileEngine
Hey guys, nice to meet you.
I am working in my current company in this project. I've written everything you'll see here. Anyway, the structure f the project and the main libraries we use were defined by another developer with more seniority than mine.
I know the project still lacks many of the main tools a RN app has ( implementation of `redux`, connection with an API, etc.), but this is the first project I start from scratch, and I will be implementing them in the next weeks.
I've seen the exercise you sent me. I didn't have enough time to sit and start it. I'll try to do it this week.

The app won't run, as I deleted some folders, leaving only code. This is because I can't publish any information about my company or the projects we are working on.
I also changed the main icons to AgileEngine's ones :)

Greetings, and thank you for your time!
Ernesto.


# App

This application is made with React Native and Redux, any contribution must follow that architecture.
Jest is used for testing.

## Versions

- React-Native : 0.59.8 (https://facebook.github.io/react-native/docs/0.59/getting-started)
- React : 16.8.3
- Redux : 4.0.1

## Folders structure
The app is organized with the following folders structure:
```
- app/
  |- actions/
  |- assets/
  |- components/
  |- config/
  |- containers/
  |- entities/
  |- reducers/
  |- repositories/
  |- screens/
  |- selectors/
  |- services
```

- **actions**: Redux actions for the app.
- **assets**: Assets for the app like fonts and icons.
- **components**: Custom React components created for the app.
- **config**: Configuration files for the app. Here you can find per enviroment dot config files.
- **entities**: Entities used in the app.
- **reducer**: Redux reducers for the app.
- **screens**: The different screens of the app.
- **selectors**: Selectors created with Reselect library to get information from the state
- **services**: Custom services for the app. Here you can find the Api service that is in charge of the communication between the app and the WS.

## Navigation
The app's navigation is handled using the react-navigation library. The navigators for the different sections of the app are defined under the app/navigation/navigators folder.

Install dependencies

Go to project folder and install node_modules by running:
```sh
yarn install
```

Go to `app/config` folder. There is an example environment file. You must create your own. For example .env.dev with content:

baseURL=**