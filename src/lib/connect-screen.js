import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ActionsCreator from '../actions';

function mapDispatchToProps(dispatch) {
	return bindActionCreators(ActionsCreator, dispatch);
}

export function connectScreen(mapStateToProps) {
	return connect(mapStateToProps, mapDispatchToProps);
}
