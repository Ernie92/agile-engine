import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { middleware as reduxPackMiddleware } from 'redux-pack';
import reducers from './reducers/index';

export const store = createStore(
	() => {},
	applyMiddleware(
		thunk,
		reduxPackMiddleware
	)
);
