import React from 'react';
import {
	createAppContainer,
	createBottomTabNavigator,
	createSwitchNavigator,
	createStackNavigator
} from 'react-navigation';

import Welcome from './screens/Welcome';
import Login from './screens/Login';
import Register from './screens/Register';
import Home from './screens/Home';
import SwitchTeam from './screens/SwitchTeam';
import Profile from './screens/Profile';

import HomeIcon from './components/icons/tab-bar/home-icon/HomeIcon';
import TabBarIcon from './components/icons/tab-bar/tab-bar-icon/TabBarIcon';

const AppNavigator = createBottomTabNavigator(
	{
		switchTeam: {
			screen: SwitchTeam,
			navigationOptions: {
				tabBarIcon: ({ focused }) => <TabBarIcon icon="switch-team" isFocused={focused} />
			}
		},
		home: {
			screen: Home,
			navigationOptions: {
				tabBarIcon: ({ focused }) => <HomeIcon isFocused={focused} />
			}
		},
		profile: {
			screen: Profile,
			navigationOptions: {
				tabBarIcon: ({ focused }) => <TabBarIcon icon="profile" isFocused={focused} />
			}
		}
	},
	{
		initialRouteName: 'home',
		tabBarOptions: {
			showLabel: false
		}
	}
);

const AuthenticationNavigator = createStackNavigator(
	{
		welcome: {
			screen: Welcome,
			navigationOptions: {
				header: null
			}
		},
		login: {
			screen: Login
		},
		register: {
			screen: Register
		}
	}
);

const RootNavigator = createSwitchNavigator(
	{
		main: AppNavigator,
		welcome: AuthenticationNavigator
	},
	{
		initialRouteName: 'welcome'
	}
);
export default createAppContainer(RootNavigator);
