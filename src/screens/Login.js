import React, { Component } from 'react';
import { ImageBackground } from 'react-native';

import InputField from '../components/input-fields/input-field/InputField';
import Button from '../components/button/Button';
import Spacing from '../components/spacing/Spacing';
import PasswordInputField from '../components/input-fields/password/PasswordInputField';
import Typography from '../components/typography/Typography';

import { containerStyles } from '../styles/globals';

const styles = {
	imageBackground: {
		dimensions: {
			height: '100%',
			width: '100%'
		},
		opacity: {
			opacity: 0.5
		}
	}
};

class Login extends Component {
	render() {
		const { navigation } = this.props;
		return (
			<ImageBackground
				source={require('../assets/images/authentication/background.png')}
				style={[containerStyles, styles.imageBackground.dimensions]}
				imageStyle={styles.imageBackground.opacity}
			>
				<Typography variant="title">Login</Typography>
				<Spacing size="large" />
				<InputField title="Email:" />
				<PasswordInputField />
				<Spacing size="large" />
				<Button title="LOGIN" onPress={() => navigation.navigate('home')} />
			</ImageBackground>
		);
	}
}

export default Login;
