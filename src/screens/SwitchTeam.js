import React, { Component } from 'react';
import { View } from 'react-native';
import { containerStyles } from '../styles/globals';
import Typography from '../components/typography/Typography';

class SwitchTeam extends Component {
	render() {
		return (
			<View style={[containerStyles, { backgroundColor: 'gold' }]}>
				<Typography variant="title">Switch team screen</Typography>
			</View>
		);
	}
}

export default SwitchTeam;
