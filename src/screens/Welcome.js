import React, { Component } from 'react';
import {
	View,
	Image,
	ImageBackground
} from 'react-native';

import Spacing from '../components/spacing/Spacing';
import Button from '../components/button/Button';
import Typography from '../components/typography/Typography';

import { containerStyles } from '../styles/globals';

const styles = {
	imageBackground: {
		dimensions: {
			height: '100%',
			width: '100%'
		},
		opacity: {
			opacity: 0.5
		}
	},
	titleContainer: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	styleBarContainer: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	styleBar: {
		height: 1,
		flex: 1,
		backgroundColor: 'black'
	}
};

class Welcome extends Component {
	render() {
		const { navigation } = this.props;
		return (
			<ImageBackground
				source={require('../assets/images/authentication/background.png')}
				style={[containerStyles, styles.imageBackground.dimensions]}
				imageStyle={styles.imageBackground.opacity}
			>
				<Spacing size="x-large" />
				<Spacing size="x-large" />
				<View style={styles.titleContainer}>
					<Typography color="black" variant="title">Welcome to</Typography>
					<Spacing size="small" direction="horizontal" />
					<Image source={require('../assets/images/logo.png')} style={{ height: 60, width: 135 }} />
				</View>
				<Spacing size="x-large" />
				<Spacing size="x-large" />
				<Spacing size="x-large" />
				<Button title="LOGIN" onPress={() => navigation.navigate('login')} />
				<Spacing size="tiny" />
				<View style={styles.styleBarContainer}>
					<View style={[styles.styleBar, { marginLeft: 16 }]} />
					<Typography variant="regular" textStyles={{ marginHorizontal: 4 }}>OR</Typography>
					<View style={[styles.styleBar, { marginRight: 16 }]} />
				</View>
				<Spacing size="tiny" />
				<Button title="REGISTER" onPress={() => navigation.navigate('register')} />
			</ImageBackground>
		);
	}
}

export default Welcome;
