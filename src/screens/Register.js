import React, { Component } from 'react';
import { ImageBackground, ScrollView } from 'react-native';

import InputField from '../components/input-fields/input-field/InputField';
import Button from '../components/button/Button';
import Spacing from '../components/spacing/Spacing';
import PasswordInputField from '../components/input-fields/password/PasswordInputField';

import { containerStyles } from '../styles/globals';
import Typography from '../components/typography/Typography';

const styles = {
	imageBackground: {
		dimensions: {
			height: '100%',
			width: '100%'
		},
		opacity: {
			opacity: 0.5
		}
	}
};

class Register extends Component {
	render() {
		const { navigation } = this.props;
		return (
			<ScrollView>
				<ImageBackground
					source={require('../assets/images/authentication/background.png')}
					style={[containerStyles, styles.imageBackground.dimensions]}
					imageStyle={styles.imageBackground.opacity}
				>
					<Spacing size="large" />
					<Typography variant="title">Create team</Typography>
					<Spacing size="large" />
					<InputField title="Team name:" />
					<InputField title="Name of your user:" />
					<InputField title="Team URL:" />
					<InputField title="Website:" />
					<InputField title="Name:" />
					<InputField title="Email:" />
					<PasswordInputField />
					<Spacing size="large" />
					<Button title="END REGISTRATION" onPress={() => navigation.navigate('home')} />
					<Spacing size="large" />
				</ImageBackground>
			</ScrollView>
		);
	}
}

export default Register;
