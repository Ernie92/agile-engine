import React, { Component } from 'react';
import { View, YellowBox } from 'react-native';
import { containerStyles } from '../styles/globals';
import Typography from '../components/typography/Typography';

YellowBox.ignoreWarnings(['Remote debugger is in a background']);

export default class HomeScreen extends Component {
	render() {
		return (
			<View style={[containerStyles, { backgroundColor: 'black' }]}>
				<Typography variant="title">Home screen</Typography>
			</View>
		);
	}
}
