import React, { Component } from 'react';
import { View, Button } from 'react-native';
import { containerStyles } from '../styles/globals';
import Typography from '../components/typography/Typography';

class Profile extends Component {
	render() {
		const { navigation } = this.props;
		return (
			<View style={[containerStyles, { backgroundColor: 'blue' }]}>
				<Typography variant="title">Profile screen</Typography>
				<Button title="LOGOUT" onPress={() => navigation.navigate('welcome')} />
			</View>
		);
	}
}

export default Profile;
