import { fonts } from './globals';

const fontStylesFor = (fontFamily, fontSize, lineHeight, letterSpacing) => ({
	fontFamily,
	fontSize,
	lineHeight,
	letterSpacing
});

export default {
	'title': fontStylesFor(fonts.GothamBold, 32, 36, 0),
	'button': fontStylesFor(fonts.Gotham, 16, 18, 0),
	'input-title': fontStylesFor(fonts.Gotham, 18, 20, 0),
	'regular': fontStylesFor(fonts.Gotham, 16, 18, 0),
	'regular-bold': fontStylesFor(fonts.GothamSuperBold, 16, 18, 0)
};
