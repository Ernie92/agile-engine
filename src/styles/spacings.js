export default {
	'tiny': 8,
	'small': 16,
	'base': 24,
	'large': 32,
	'x-large': 40
};
