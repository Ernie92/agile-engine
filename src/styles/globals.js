import colors from './palette';
import spacings from './spacings';

export const containerStyles = {
	flex: 1,
	backgroundColor: colors.white,
	paddingHorizontal: spacings.small
};

export const fonts = {
	GothamSuperBold: 'Gotham-Black',
	GothamBold: 'Gotham-Bold',
	GothamBoldItalic: 'Gotham-BoldItalic',
	GothamBook: 'Gotham-Book',
	GothamBookItalic: 'Gotham-BookItalic',
	GothamLight: 'Gotham-Light',
	GothamLightItalic: 'Gotham-LightItalic',
	Gotham: 'Gotham-Medium',
	GothamItalic: 'Gotham-MediumItalic',
	GothamThin: 'Gotham-Thin',
	GothamThinItalic: 'Gotham-ThinItalic',
	GothamUltraItalic: 'Gotham-UltraItalic',
	GothamXLight: 'Gotham-XLight',
	GothamXLightItalic: 'Gotham-XLightItalic'
};
