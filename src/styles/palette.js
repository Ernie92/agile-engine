export default {
	white: '#FFFFFF',
	lightGray: '#D8D8D8',
	disabledGray: '#D9D9D9',
	gray: '#A4A6A9',
	yellow: '#F5BA2B',
	green: '#4EBD30',
	black: '#404041'
};
