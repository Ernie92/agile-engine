const icons = {
	'home': require('../assets/icons/tab-bar/home.png'),
	'switch-team': require('../assets/icons/tab-bar/switch-team.png'),
	'profile': require('../assets/icons/tab-bar/profile.png')
};

export default iconName => icons[iconName];
