import colors from '../../styles/palette';

export default {
	container: {
		height: 50,
		borderRadius: 25,
		alignItems: 'center',
		justifyContent: 'center'
	},
	enabledBackgroundColor: { backgroundColor: colors.yellow },
	disabledBackgroundColor: { backgroundColor: colors.disabledGray }
};
