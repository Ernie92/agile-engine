import React from 'react';
import { TouchableOpacity } from 'react-native';

import PropTypes from 'prop-types';

import Typography from '../typography/Typography';

import styles from './Styles';

const Button = ({ title, isEnabled, onPress }) => (
	<TouchableOpacity
		style={[
			styles.container,
			isEnabled ? styles.enabledBackgroundColor : styles.disabledBackgroundColor
		]}
		disabled={!isEnabled}
		onPress={onPress}
		activeOpacity={0.8}
	>
		<Typography variant="button" color="white">{title}</Typography>
	</TouchableOpacity>
);

Button.propTypes = {
	title: PropTypes.string,
	isEnabled: PropTypes.bool,
	onPress: PropTypes.func.isRequired
};

Button.defaultProps = {
	title: 'Button',
	isEnabled: true
};

export default Button;
