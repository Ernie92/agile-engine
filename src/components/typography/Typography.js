import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';

import colors from '../../styles/palette';
import variants from '../../styles/typography';

const Typography = ({
	color,
	variant,
	children,
	textStyles
}) => (
	<Text
		style={{
			color: colors[color],
			...variants[variant],
			...textStyles
		}}
	>
		{ children }
	</Text>
);

Typography.propTypes = {
	color: PropTypes.string,
	variant: PropTypes.string.isRequired,
	children: PropTypes.node.isRequired,
	textStyles: PropTypes.instanceOf(Object)
};

Typography.defaultProps = {
	color: 'black',
	textStyles: {}
};

export default Typography;
