import colors from '../../../styles/palette';

export default {
	focusedStyleBar: {
		height: 2,
		width: 24,
		backgroundColor: colors.yellow
	}
};
