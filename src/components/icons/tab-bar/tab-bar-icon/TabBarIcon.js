import React, { Fragment } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

import Icon from '../../icon/Icon';
import Spacing from '../../../spacing/Spacing';

import styles from '../TabBarStyles';

const TabBarIcon = ({ icon, size, isFocused }) => (
	<View>
		<Icon icon={icon} size={size} />
		{
			isFocused && (
				<Fragment>
					<Spacing size="tiny" />
					<View style={styles.focusedStyleBar} />
				</Fragment>
			)
		}
	</View>
);

TabBarIcon.propTypes = {
	icon: PropTypes.string.isRequired,
	size: PropTypes.number,
	isFocused: PropTypes.bool
};

TabBarIcon.defaultProps = {
	size: 24,
	isFocused: false
};

export default TabBarIcon;
