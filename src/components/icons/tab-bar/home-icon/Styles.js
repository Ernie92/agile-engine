import colors from '../../../../styles/palette';
import tabBarStyles from '../TabBarStyles';

const DEVIATION = -20;
const ICON_HEIGHT = 60;

export default {
	backgroundContainer: {
		height: ICON_HEIGHT * 1.2, // I added this * 1.2 as the original icon fitted well, but this didn't happen with AE's icon :)
		width: ICON_HEIGHT * 1.2,
		borderRadius: ICON_HEIGHT * 1.2 / 2,
		backgroundColor: colors.white,
		alignItems: 'center',
		justifyContent: 'center',
		top: DEVIATION
	},
	iconHeight: ICON_HEIGHT,
	iconExtraStyles: {
		width: ICON_HEIGHT
	},
	focusedStyleBar: {
		...tabBarStyles.focusedStyleBar,
		top: DEVIATION,
		alignSelf: 'center'
	}
};
