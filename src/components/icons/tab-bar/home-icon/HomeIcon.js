import React, { Fragment } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

import Icon from '../../icon/Icon';
import Spacing from '../../../spacing/Spacing';

import styles from './Styles';

const HomeIcon = ({ isFocused }) => (
	<Fragment>
		<View style={styles.backgroundContainer}>
			<Icon
				icon="home"
				size={styles.iconHeight}
				extraStyles={styles.iconExtraStyles}
			/>
		</View>
		{
			isFocused && (
				<Fragment>
					<Spacing size="tiny" />
					<View style={styles.focusedStyleBar} />
				</Fragment>
			)
		}
	</Fragment>
);

HomeIcon.propTypes = {
	isFocused: PropTypes.bool
};

HomeIcon.defaultProps = {
	isFocused: false
};

export default HomeIcon;
