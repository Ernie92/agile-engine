import React from 'react';
import { Image } from 'react-native';
import PropTypes from 'prop-types';

import getIconByName from '../../../utils/get-icon-by-name';

const Icon = ({ icon, size, extraStyles }) => (
	<Image style={{ width: size, height: size, ...extraStyles }} source={getIconByName(icon)} />
);

Icon.propTypes = {
	icon: PropTypes.string.isRequired,
	size: PropTypes.number,
	extraStyles: PropTypes.object
};

Icon.defaultProps = {
	size: 24,
	extraStyles: {}
};

export default Icon;
