import React from 'react';

import InputField from '../input-field/InputField';

const PasswordInputField = () => <InputField title="Password" secureTextEntry />;

export default PasswordInputField;
