import colors from '../../../styles/palette';
import spacings from '../../../styles/spacings';
import variants from '../../../styles/typography';

const TEXT_INPUT_HEIGHT = 40;

export default {
	textInputContainer: {
		borderBottomWidth: 1,
		height: TEXT_INPUT_HEIGHT,
		paddingHorizontal: spacings.small / 2,
		...variants.regular
	},
	selectionColor: colors.yellow,
	unselectedColor: colors.lightGray

};
