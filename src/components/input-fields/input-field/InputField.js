import React, { Component, Fragment } from 'react';
import { TextInput } from 'react-native';
import PropTypes from 'prop-types';

import Spacing from '../../spacing/Spacing';

import styles from './Styles';
import Typography from '../../typography/Typography';

class InputField extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isFocused: false
		};
	}

	render() {
		const { title, secureTextEntry } = this.props;
		const { isFocused } = this.state;
		return (
			<Fragment>
				<Typography
					variant={isFocused ? 'regular-bold' : 'regular'}
					color={isFocused ? 'yellow' : 'gray'}
				>
					{title}
				</Typography>
				<Spacing size="tiny" />
				<TextInput
					style={[
						styles.textInputContainer,
						{ borderBottomColor: isFocused ? styles.selectionColor : styles.unselectedColor }
					]}
					onFocus={() => { this.setState({ isFocused: true }); }}
					onBlur={() => { this.setState({ isFocused: false }); }}
					selectionColor={styles.selectionColor}
					secureTextEntry={secureTextEntry}
				/>
				<Spacing size="tiny" />
			</Fragment>
		);
	}
}

InputField.propTypes = {
	title: PropTypes.string,
	secureTextEntry: PropTypes.bool
};

InputField.defaultProps = {
	title: undefined,
	secureTextEntry: false
};

export default InputField;
