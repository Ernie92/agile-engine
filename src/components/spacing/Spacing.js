import React from 'react';
import { View } from 'react-native';

import PropTypes from 'prop-types';

import spacings from '../../styles/spacings';

const Spacing = ({ size, direction }) => {
	const spacingSize = spacings[size];
	const height = direction === 'vertical' ? spacingSize : 'auto';
	const width = direction === 'vertical' ? 'auto' : spacingSize;

	return (
		<View style={{ width, height }} />
	);
};

Spacing.propTypes = {
	size: PropTypes.string.isRequired,
	direction: PropTypes.oneOf(['vertical', 'horizontal'])
};

Spacing.defaultProps = {
	direction: 'vertical'
};

export default Spacing;
